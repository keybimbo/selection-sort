public static int IntArrayMin (int[] data, int start)
{
	int minPos = start; 
	for (int pos=start+1; pos < data.Length; pos++){
		if (array [pos] < array [minPos]) minPos = pos;
	}
	return minPos; 
}

public static void IntArraySelectionSort (int[] array)
{
	int i;
	int N = array.Length;

	for (i=0; i < N-1; i++) {
		int k = IntArrayMin (array, i);
		if (i != k) exchange (array, i, k);
	}
}